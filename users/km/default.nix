{ config, inpits, lib, pkgs, ... }:

let
  theme = import ../../config/theme.nix;
in
{
  fonts.fontconfig.enable = true;

  gtk = {
    enable = true;
    font.name = "Sarasa Gothic J";

    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus-Dark";
    };

    theme = {
      package = pkgs.phocus;
      name = "phocus";
    };
  };

  home = {
    file = {
      ".local/bin/can" = {
        executable = true;
        text = import ./scripts/can.nix;
      };

      # ".local/bin/ccolor" = {
      #   executable = true;
      #   text = import ./scripts/ccolor.nix { inherit pkgs; };
      # };

      # ".local/bin/palette" = {
      #   executable = true;
      #   text = import ./scripts/palette.nix;
      # };

      # ".local/bin/screenshot" = {
      #   executable = true;
      #   text = import ./scripts/screenshot.nix { inherit theme; };
      # };

      # ".local/bin/showcase" = {
      #   executable = true;
      #   text = import ./scripts/showcase.nix;
      # };

      # ".local/bin/volume" = {
      #   executable = true;
      #   text = import ./scripts/volume.nix;
      # };

      # ".config/qt5ct/colors/Horizon.conf".source = ./config/Horizon.conf;

      # ".icons/default".source = "${
      #   if theme.lightModeEnabled
      #   then "${pkgs.vanilla-dmz}/share/icons/Vanilla-DMZ"
      #   else "${pkgs.vanilla-dmz}/share/icons/Vanilla-DMZ-AA"
      # }";
    };


    homeDirectory = "/home/${config.home.username}";
    username = "km";
  
    packages = with pkgs; [
      agenix
      betterdiscord-installer
      cargo
      celluloid
      discord
      dragon-drop
      element-desktop
      ffmpeg
      flavours
      firefox
      font-manager
      gimp
      gitAndTools.gh
      glava
      gotop
      graphviz
      neofetch
      nixpkgs-fmt
      nixpkgs-review
      nur.repos.fortuneteller2k.impure.eww
      pfetch
      python3
      rustc
      rust-analyzer
      rnix-lsp
      signal-desktop
      speedtest-cli
      spotify
      freerdp
      tdesktop
    ];
  
    sessionPath = [
      "${config.xdg.configHome}/emacs/bin"
      "${config.xdg.configHome}/scripts"
      "${config.home.homeDirectory}/.local/bin"
    ];
  
    sessionVariables = {
      EDITOR = "${config.programs.neovim.package}/bin/nvim";
      GOPATH = "${config.home.homeDirectory}/Extras/go";
      MANPAGER = "${config.programs.neovim.package}/bin/nvim +Man!";
      RUSTUP_HOME = "${config.home.homeDirectory}/.local/share/rustup";
    };
  
    stateVersion = "21.05";
  };

  programs = {
    alacritty = {
      enable = true;
      settings = import ./config/alacritty.nix { inherit theme; };
    };

    bat = {
      enable = true;
      config = {
        pager = "never";
        style = "platin";
        theme = "base16";
      };
    };

    dircolors = {
      enable = true;
      settings = pkgs.lib.mkForce { };
      extraConfig = import ./config/dircolors.nix;
    };

    direnv = {
      enable = true;
      enableNixDirenvIntegration = true;
    };

    emacs = {
      enable = true;
      package = pkgs.emacsGcc;
    };

    exa = {
      enable = true;
      enableAliases = true;
    };

    home-manager.enable = true;

    htop = {
      enable = true;

      settings = {
        detailed_cpu_time = true;
        hide_kernel_threads = false;
        show_cpu_frequency = true;
        show_cpu_usage = true;
        show_program_path = false;
        show_thread_names = true;
      }
      // (with config.lib.htop; leftMeters {
        AllCPUs = modes.Bar;
        Memory = modes.Bar;
        Swap = modes.Bar;
        Zram = modes.Bar;
      })
      // (with config.lib.htop; rightMeters {
        Tasks = modes.Text;
        LoadAverage = modes.Text;
        Uptime = modes.Text;
        Systemd = modes.Text;
      });
    };

    neovim = {
      enable = true;
      package = pkgs.neovim-nightly;
      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;
      withNodeJs = true;
      extraConfig = import ./config/neovim;
      extraPackages = with pkgs; [ rnix-lsp shellcheck ];

      plugins = import ./config/neovim/plugins.nix {
        inherit pkgs;
        inherit (theme) colors;
      };
    };

    nix-index.enable = true;
    rofi.enable = true;

    starship = {
      enable = true;
      settings = import ./config/starship.nix;
    };

    vscode =
      let
        extraPackages = pkgs: with pkgs; [
          rustup
        ];
      in
      {
        enable = true;
        package = pkgs.vscode.fhsWithPackages (pkgs: extraPackages pkgs);
      };
    
    zathura = {
      enable = true;
      extraConfig = "map <C-i> recolor";
      options = import ./config/zathura.nix { inherit theme;};
    };

    zsh = rec {
      enable = true;
      autocd = true;
      enableAutosuggestions = true;
      dotDir = ".config/zsh";

      history = {
        expireDuplicatesFirst = true;
        extended = true;
        path = "${config.programs.zsh.dotDir}/zsh_history";
        save = 50000;
      };

      initExtra = import ./config/zsh {
        inherit dotDir;
        inherit (config) home;
      };

      plugins = import ./config/zsh/plugins.nix { inherit pkgs; };
      shellAliases = import ./config/sh-aliases.nix;
    };
  };
  systemd.user.startServices = true;

  xdg = {
    enable = true;

    configFile = {
      "nvim/coc-settings.json".source =
        let
          json = pkgs.formats.json { };
          neovim-coc-settings = import ./config/neovim/coc-settings.nix { inherit pkgs; };
        in
        json.generate "coc-settings.json" neovim-coc-settings;
    };

    userDirs = {
      enable = true;
      documents = "${config.home.homeDirectory}/Extras/Documents";
      music = "${config.home.homeDirectory}/Media/Music";
      pictures = "${config.home.homeDirectory}/Media/Pictures";
      videos = "${config.home.homeDirectory}/Media/Videos";
    };
  };

  xresources.extraConfig = import ./config/xresources.nix { inherit theme; };
}
