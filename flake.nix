{
  description = "My NixOS Config";

  inputs = {
    agenix.url = "github:ryantm/agenix";
    emacs.url = "github:nix-community/emacs-overlay";
    home.url = "github:nix-community/home-manager";
    neovim.url = "github:nix-community/neovim-nightly-overlay";
    nur.url = "github:nix-community/NUR";
    review.url = "github:Mic92/nixpkgs-review";
    rust.url = "github:oxalica/rust-overlay";

    # nixpkgs branches
    master.url = "github:nixos/nixpkgs/master";
    stable.url = "github:nixos/nixpkgs/release-21.05";
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    # default nixpkgs for packages and modules
    nixpkgs.follows = "master";
  };

  outputs = { self, agenix, home, nixpkgs, ... } @ inputs:
    with nixpkgs.lib;
    let
      config = {
        allowBroken = true;
        allowUnfree = true;
      };

      filterNixFiles = k: v: v == "regular" && hasSuffix ".nix" k;

      importNixFiles = path: (lists.forEach (mapAttrsToList (name: _: path + ("/" + name))
        (filterAttrs filterNixFiles (builtins.readDir path)))) import;

      overlays = with inputs; [
        (final: _:
          let
            system = final.stdenv.hostPlatform.system;
          in
          {
            # input packages
            agenix = agenix.defaultPackage.${system};
            neovim-nightly = neovim.packages.${system}.neovim;
            nixpkgs-review = review.defaultPackage.${system};

            # nixpkgs branches
            master = import master { inherit config system; };
            unstable = import unstable { inherit config system; };
            stable = import stable { inherit config system; };
            staging = import staging { inherit config system; };
            staging-next = import staging-next { inherit config system; };

          })
        # input overlays
        emacs.overlay
        nur.overlay
        rust.overlay
      ]
      # overlays from ./overlays directory
      ++ (importNixFiles ./overlays);
    in
    {
      nixosConfigurations.exegol = import ./hosts/exegol {
        inherit config agenix home inputs nixpkgs overlays;
      };

      exegol = self.nixos.Configurations.exegol.config.system.build.toplevel;
    };
}

