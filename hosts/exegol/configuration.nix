{ config, lib, pkgs, options, ... }:

let theme = import ../../config/theme.nix;
in
{
  boot = {
    kernelPackages = pkgs.linuxPackages_xanmod;

    loader = {
      efi.canTouchEfiVariables = true;

      grub = {
        device = "nodev";
        enable = true;
        efiSupport = true;
        version = 2;
        gfxmodeEfi = "1920x1080";
        gfxpayloadEfi = "keep";
        useOSProber = true;
      };
    };
  };

  console = 
    {
      font = "Lat2-Terminus16";
      keyMap ="de-latin1-nodeadkeys";
    };

  hardware = {
    cpu = {
      amd.updateMicrocode = true;
    };

    enableAllFirmware = true;
    enableRedistributableFirmware = true;

    opengl = {
      enable = true;
      driSupport = true;
      extraPackages = with pkgs; [ ];
    };
    pulseaudio.enable = false;
  };

  imports = [
    ./hardware-configuration.nix
  ];

  i18n.defaultLocale = "de_AT.UTF-8";

  environment = {

    systemPackages = with pkgs; [
      caffeine-ng
      coreutils
      curl
      fd
      file
      git
      glxinfo
      gnome.gnome-tweaks
      imagemagick
      jp2a
      libtool
      libva-utils
      lm_sensors
      ntfs3g
      pandoc
      pavucontrol
      pciutils
      psmisc
      ripgrep
      shellcheck
      shotgun
      unrar
      unzip
      util-linux
      wget
      zip
    ];
  };

  fonts = {
    fonts = with pkgs; [
      cozette
      curie
      dejavu_fonts
      emacs-all-the-icons-fonts
      etBook
      fantasque-sans-mono
      fira-code
      fira-code-symbols
      inter
      iosevka
      inconsolata
      mplus-outline-fonts
      nerdfonts
      nur.repos.fortuneteller2k.iosevka-ft-bin
      roboto-mono
      sarasa-gothic
      scientifica
      symbola
      terminus_font
      twemoji-color-font
      xorg.fontbh100dpi
    ];
    
    fontconfig = {
      enable = true;

      defaultFonts = {
        serif = [
          "Sarasa Gothic C"
          "Sarasa Gothic J"
          "Sarasa Gothic K"
        ];

        sansSerif = [
          "Sarasa Gothic C"
          "Sarasa Gothic J"
          "Sarasa Gothic K"
        ];

        monospace = [
          "Iosevka FT"
          "Iosevka Nerd Font"
          "Sarasa Mono C"
          "Sarasa Mono J"
          "Sarasa Mono K"
        ];

        emoji = [ "Twitter Color Emoji" ];
      };
    };
  };

  networking = {
    dhcpcd.enable = false;
    hostName = "exegol";

    interfaces = {
      enp4s0.useDHCP = true;
      wlp5s0.useDHCP = true;
    };

    nameservers = [
      "172.19.20.254"
      "172.19.0.254"
      "1.1.1.1"
      "1.0.0.1"
    ];

    networkmanager = {
      enable = true;
      dns = "none";
    };

    useDHCP = false;
  };

  powerManagement.cpuFreqGovernor = "performance";

  programs = {
    mosh = {
      enable = true;
    };

    steam = {
      enable = true;
    };
  };

  security = {
    protectKernelImage = true;
    rtkit.enable = true;
    sudo.enable = false;
    sudo.wheelNeedsPassword = false;

    doas = {
      enable = true;
      wheelNeedsPassword = false;
      extraRules = [{
        users = [ "km" ];
        keepEnv = true;
        persist = true;
      }];
    };
  };

  services = {
    auto-cpufreq.enable = true;

    chrony = {
      enable = true;

      servers = [
        "bevtime1.metrologie.at"
        "bevtime2.metrologie.at"
        "time.metrologie.at"
        "ts1.univie.ac.at"
        "ts2.unicie.ac.at"
        "0.at.pool.ntp.org"
        "1.at.pool.ntp.org"
        "2.at.pool.ntp.org"
        "3.at.pool.ntp.org"
      ];
    };

    irqbalance.enable = true;
    journald.extraConfig = lib.mkForce "";

    openssh = {
      enable = true;
      gatewayPorts = "yes";
      permitRootLogin = "yes";
    };

    picom = {
      enable = false;
      refreshRate = 60;
      experimentalBackends = true;
      backend = "glx";
      vSync = true;

      settings = import ./config/picom-settings.nix;
    };

    pipewire = {
      enable = true;
      
      alsa = {
        enable = true;
        support32Bit = true;
      };

      jack.enable = true;
      pulse.enable = true;

      # config = import ./config/pipewire;
      # media-session.config = import ./config/pipewire/media-session.nix;
    };
    
    xserver = {
      enable = true;
      
      layout = "de";
      xkbModel = "pc105";
      xkbOptions = "nodeadkeys";

      videoDrivers = [ "nvidia" ];     
 
      displayManager = {
        gdm = {
          enable = config.services.xserver.enable;
          wayland = false;
        };
      };

      desktopManager.gnome.enable = true;

      windowManager = {
        xmonad = with pkgs; {
          enable = false;
          config = import ./config/xmonad.nix { inherit config pkgs theme; };
          extraPackages = hpkgs: with hpkgs; [ dbus xmonad-contrib ];

          ghcArgs = [
          ];
        };
      };
    };
  };

  system = {
    stateVersion = "21.05";
  };

  systemd = {
    extraConfig = "RebootWatchdogSec=5";
    services.rtkit-daemon = import ./services/rtkit-daemon.nix { inherit pkgs; };

    user.services = {
      pipewire.wantedBy = [ "default.target" ];
      pipewire-pulse.wantedBy = [ "default.target" ];
    };
  };

  time.timeZone = "Europe/Vienna";

  users.users.km = {
    isNormalUser = true;
    home = "/home/km";
    shell = pkgs.zsh;

    extraGroups = [
      "wheel"
      "networkmanager"
      "video"
      "audio"
      "realtime"
    ];
  };

  zramSwap = {
    enable = true;
    memoryPercent = 100;
  };
}

